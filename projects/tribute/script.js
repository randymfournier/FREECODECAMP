$(document).ready();

// $('.play').hide();
$('.pause').click(function() {
  document.getElementById('player').pause()
  $('.pause').css("visibility", "hidden");
  $('.play').css("visibility", "visible");
});
$('.play').click(function() {
  document.getElementById('player').play()
  $('.play').css("visibility", "hidden");
  $('.pause').css("visibility", "visible");
});
